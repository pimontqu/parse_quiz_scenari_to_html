from lxml import etree
import os
import re

# set namespace scenari
def set_namespace_scenari():
    return namespaces
# parse les formule mathématiques
def parseLatex(xml):
    # parse les balise xml qui pour role d'afficher des nombre ou lettre en indice
    parse = re.sub(r'<sc:textLeaf role=\"ind\">(.+?)</sc:textLeaf>', r'<sub>\g<1></sub>', xml)
    parse = re.sub(r'<sc:textLeaf role=\"ind\" sc:id=\"[\w]+?\">(.+?)</sc:textLeaf>', r'<sub>\g<1></sub>', parse)
    # parse les balise xml qui pour role d'afficher des nombre ou lettre en exposant
    parse = re.sub(r'<sc:textLeaf role=\"exp\">(.+?)</sc:textLeaf>', r'<sup>\g<1></sup>', parse)
    parse = re.sub(r'<sc:textLeaf role=\"exp\" sc:id=\"[\w]+?\">(.+?)</sc:textLeaf>', r'<sup>\g<1></sup>', parse)
    # parse les balise xml contenant du latex
    parse = re.sub(r'<sc:textLeaf role=\"mathtex\">(.+?)</sc:textLeaf>', r'$$\\begin{matrix}\g<1>\\end{matrix}$$', parse)
    parse = re.sub(r'<sc:textLeaf role=\"mathtex\">([\n\t ]*.*[\n ]*?)</sc:textLeaf>', r'$$\\begin{matrix}\g<1>\\end{matrix}$$', parse)
    parse = re.sub(r'<sc:textLeaf role=\"mathtex\" sc:id=\"[\w]+?\">(.+?)</sc:textLeaf>', r'$$\\begin{matrix}\g<1>\\end{matrix}$$', parse)
    parse = re.sub(r'<sc:textLeaf role=\"mathtex\" sc:id=\"[\w]+?\">([\n\t ]*.*[\n ]*?)</sc:textLeaf>', r'$$\\begin{matrix}\g<1>\\end{matrix}$$', parse)
    # parse les balise restante (dans certain cas il en reste car le parse prend en compte plusieurs d'un coup)
    parse = re.sub(r'<sc:textLeaf role=\"mathtex\">', r'$$\\begin{matrix}', parse)
    parse = re.sub(r'</sc:textLeaf>', r'\\end{matrix}$$', parse)
    # enleve certaine balise latex vide.
    parse = re.sub(r'<sc:textLeaf role="(ind|exp|mathtex)"/>', r'', parse)
    return parse
# création text d'ennonce sans les balise img
def createPreview(html):
    preview = re.sub(r'<img class=\"img-fluid\" src=\"[\w/0-9-._ ,()]+\" alt=\"[\w/0-9-._ ,()]+\"/>', r'', html)
    return preview
# parse les tables xml en html
def parseTable(xml):
    # parse les balises tables xml en balises table html
    parse = re.sub(r'<sc:table[\w\" =:]*>', r'<table class="table">', xml)    
    parse = re.sub(r'</sc:table[\w\" =:]*>', r'</table>', parse)
    # parse le titre du tableau
    parse = re.sub(r'<(/){0,1}sc:caption[\w\" =:]*>', r'<\g<1>caption>', parse)
    # parse les en tête du tableau en thead html
    parse = re.sub(r'<sc:row role=\"head\">([\n\t\w<>:\"=/ ]+?)</sc:row>', r'<thead class="thead-default"><tr>\g<1></tr></thead>', parse)
    # récupère tous les header du xml
    allThead = re.findall(r'<thead', parse)
    if(len(allThead) > 0):
        # découpe le xml en liste contenant toutes les ligne
        allLines = parse.split('\n')
        # indiquera si on et dans un thead
        isTh = False
        # increment pour la boucle
        line = 0
        # pour chaque ligne
        while line < len(allLines):
            # si sur la ligne courante il y une balise ouvrante thead
            if('<thead' in allLines[line]):
                # on indique qu'on est dans un header
                isTh = True
            # si on sur la lgine courante il y à une balise fermante thead
            if('</tr></thead>' in allLines[line]):
                # on indique qu'on est sortit du header
                isTh = False
            # si un on est dans un header et sur la ligne courante il y une ou des balises xml sc:cell
            if(isTh and 'sc:cell' in allLines[line]):
                # alors on remplace ces balise par th html
                allLines[line] = re.sub(r'sc:cell', r'th', allLines[line])
            # on incrémente le numero de la ligne
            line += 1
        # rassemble les lignes dans la même disposition
        parse = '\n'.join(allLines)
    # parse les balise sc:row en tr
    parse = re.sub(r'<(/){0,1}sc:row[\w\" =]*>', r'<\g<1>tr>', parse)
    # parse les baise sc:cell restante en td
    parse = re.sub(r'<(/){0,1}sc:cell[\w\" =]*>', r'<\g<1>td>', parse)
    # retire des balise vide inutile
    parse = re.sub(r'<sc:cell role="(word|num)"/>', '', parse)
    return parse
# parse les images en image hmtl et lie les photo à la question
def parseImg(xml, rootImg):
    # recherche des chemin des image dans le xml
    imgFullPath = re.search('sc:refUri=\"(.*?)\"', xml)
    # s'il y a des images dans le html
    if(imgFullPath is not None):
        # coupe le chemin de l'image
        imgPathSplit = imgFullPath.group(1).split('/')
        # concatene le nom de l'image avec le chemin du dossier racine des image
        newImgPath = rootImg + imgPathSplit[len(imgPathSplit) - 1]
        # on parse les balises représentant des images en html avec le nouveau chemin
        parse = re.sub(r'<(sp:res|sc:inlineImg|sp:img)[\w\"= :/.]* sc:refUri=\"([\w0-9-_.()/ ,]*)\"/{0,1}>', r'<img src="' + newImgPath + r'" alt="' + newImgPath + r'"/>', xml)
    else:
        parse = xml
    # on retourne le xml parser
    return parse
# parse les listes non ordoner xml en liste a puce html
def parseUl(xml):
    parse = re.sub(r'<(/){0,1}sc:itemizedList[\w \":=]*>', r'<\g<1>ul>', xml)
    parse = re.sub(r'<(/){0,1}sc:listItem[\w \":=]*>', r'<\g<1>li>', parse)
    return parse
# parse les liste ordonnées xml en ol html
def parseOl(xml):
    parse = re.sub(r'<(/){0,1}sc:orderedList[\w \":=]*>', r'<\g<1>ol>', xml)
    parse = re.sub(r'<(/){0,1}sc:listItem[\w \":=]*>', r'<\g<1>li>', parse)
    return parse
# enlèvele toutes les balises xml "inutile"(en surplus) pour le html et les ligne vide
def cleanXml(xml):
    clean = re.sub(r'<[/]{0,1}(sc:question|op:res|sp:txt|op:txt|op:resInfoM|sc:inlineStyle|sc:column|sc:choiceLabel){1}[\w0-9 \":=/.]*>', '', xml).strip()
    clean = re.sub(r'</sp:res>', r'', clean)
    # lors de certaines ponctuation scenarie met un espace "different" avant celle ci
    # donc je les remplace par des espaces normaux
    clean = re.sub(r'[^\w0-9-+_.\\/°. \#?=\'\"<>{}()\[\]](:|;|\?)', r' \g<1>', clean)
    # enlève les ligne vide et ajoute des saut de ligne
    clean = re.sub(r'\t*\n', '', clean)
    clean = re.sub(r'\t{2,}', '\n', clean)
    return clean
# parse les paragraphess xml en paragraphe html
def parsePara(xml, isChoice=False):
    patternPara = r'<(/){0,1}sc:para[\w \":=/0-9._-]*>'
    if(isChoice):
        contentPara = re.sub(patternPara, '',xml)
    else:
        contentPara = re.sub(patternPara, r'<\g<1>p>',xml)
    return contentPara
# parse les urls xml en urls html
def parseUrl(xml):
    parse = re.sub(r'<sc:phrase role=\"url\"><op:urlM[\w0-9-–_ :=\"/.\n]*><sp:url[\w0-9-–_ :=\"/.\n]*>(http://|https://)*([\w0-9-_. %]+\.{0,1}[a-z]+[\w/_–.\#&?=;\n%-]*)</sp:url>(<sp:title>([\w0-9-–_. \';, :\n	()/<]*)</sp:title>)*</op:urlM>([\w0-9-–_. /\';,’\'%:&=?\#\n	]*)</sc:phrase>',r'<a href="\g<1>\g<2>" alt="\g<4>">\g<5></a>', xml)
    parse = re.sub(r'<sc:phrase role=\"url\"><op:urlM[\w0-9-–_ :=\"/.\n]*><sp:url[\w0-9-–_ :=\"/.\n]*>(http://|https://)*([\w0-9-_. %]+\.{0,1}[a-z]+[\w/_–.\#&?=;\n%-]*)</sp:url>(<sp:title>([\w0-9-–_. \';, :\n	()/<]*)</sp:title>)*</op:urlM>([\w0-9-–_. /\';,’\'%:&=?\#\n	]*)(<a href=\"(http://|https://)*([\w0-9-_. %]+\.{0,1}[a-z]+[\w/_–.\#&?=;\n%-]*)\" alt=\"([\w0-9-–_. \';, :\n	()/<]*)\"></a>)</sc:phrase>', r'<a href="\g<1>\g<2>" alt="\g<4>\g<5></a>"', parse)
    return parse
# appelle toutes les fonctions de parse 
def parseAllTag(xml, rootImg, isChoice=False):
    html = parsePara(xml, isChoice)
    html = parseImg(html, rootImg)
    html = parseOl(html)
    html = parseTable(html)
    html = parseUl(html)
    html = parseUrl(html)
    html = parseLatex(html)
    return html
# parse les copyright xml en html
def parseCopyRight(cpyrgtNode):
    cpyrgtXml = etree.tostring(cpyrgtNode[0], encoding='utf8').decode('utf8')
    cpyrgtXmlParse = re.sub(r'<sc:para[a-zA-z0-9-_. /\"=:]+>\n*([\w0-9-–_. <>:=\"/ =,;\'	\n ]+)\n*</sc:para>', r'\g<1>', cpyrgtXml)
    cpyrgtXmlParse = '<p>' + cpyrgtXmlParse.strip() + '</p>'
    cpyrgtXmlsndParse = parseUrl(cpyrgtXmlParse)
    return cpyrgtXmlsndParse
# parse les choix d'une question est les lis à la question
def parseChoice(choiceNode, rootImg, solution):
    # pour chaque chaque choix de cette question
    choicesHtml = '<ul>\n'
    for choice in choiceNode:
        # on convertir la node xml du choix en string
        choiceXml = etree.tostring(choice, encoding='utf8').decode('utf8')
        # index du choix
        index_choice = choiceNode.index(choice)
        # si le type de question est un qcu
        if(solution > 0):
            type_input = 'radio'
            # si l'index du choix + 1 est égale à la solution on passe la valeurs de good à true
            if(index_choice + 1 == solution):
               is_good_answer = True
            # sinon on la passe à false
            else:
                is_good_answer = False
        else:
            type_input = 'checkbox'
            # si c'est un qcm alors on cherche le parmatre solution du choix
            isGood = re.findall(r'solution=\"(checked|unchecked)\"', choiceXml)
            # si le paramètre est égale à checked alors on passe la valeur a true
            if(isGood[0] == 'checked'):
                is_good_answer = True
            # sinon on la passe à false
            else:
                is_good_answer = False
        choiceXml = cleanXml(choiceXml)
        # on coupe le xml en deux partie (une pour le choix et l'autre pour son explication)
        choiceXmlSplit = choiceXml.split('<sc:choiceExplanation>')
        # on supprime le balise en trop
        choiceXmlClean = re.sub(r'<[/]{0,1}sc:choice[\w :=\"/.]*>', r'', choiceXmlSplit[0]).strip()
        # puis on le parse en html
        choiceHtml = parseAllTag(choiceXmlClean, rootImg, True)
        # si le choix à une explication alors
        choicesHtml += '\t<li><input type="' + type_input + '" id="" name=""/><label for="">' + choiceHtml + '</label></li>\n'
        if(len(choiceXmlSplit) == 2):
            # on enlève les balise inutile
            choiceExplainXmlClean = re.sub(r'(</sc:choiceExplanation>|</sc:choice>)', r'', choiceXmlSplit[1]).strip()
            # on le parse en html
            choiceExplainHtml = parseAllTag(choiceExplainXmlClean, rootImg)
    return choicesHtml + '</ul>'
# enregistre et parse tous les éléments des questions
def parseQuiz(xml, outDir = '', rootImg = ''):
    # namespaces scenari
    namespaces = {
        'sc' : 'http://www.utc.fr/ics/scenari/v3/core',
        'op' : 'utc.fr:ics/opale3',
        'sp' : 'http://www.utc.fr/ics/scenari/v3/primitive',
        'sfm' : 'http://www.utc.fr/ics/scenari/v3/filemeta'
    }
    # on coupe le nom du fichier xml en deux au niveaux de l'extention
    xmlPathWithoutExt = os.path.splitext(xml)[0]
    # on coupe en plusieurs parties le chemin du fichier
    xmlPathSplit = xmlPathWithoutExt.split('/')
    # on recupère le nom du fichier sans l'extention
    code = xmlPathSplit[len(xmlPathSplit) - 1]
    # on parse le fichier xml pour qu'il puisse être lue facilement par python
    tree = etree.parse(os.path.realpath(xml), parser=etree.XMLParser(remove_comments=True))
    # On recherche deux chemin de base (celui d'un qcm et d'un qcu)
    xpathQcm = tree.xpath('/sc:item/op:mcqMur', namespaces=namespaces)
    xpathQcu = tree.xpath('/sc:item/op:mcqSur', namespaces=namespaces)
    # on regarde si la question est un QCU, QCM ou un autre type
    if(len(xpathQcm) > 0):
        path = 'op:mcqMur'
    elif(len(xpathQcu) > 0):
        path = 'op:mcqSur'
    else:
        path = None
    # si la question est bien un qcu ou un qcm
    if(path is not None):
        # on récupère le titre de la querstion
        titleNode = tree.xpath('/sc:item/' + path + '/op:exeM/sp:title', namespaces=namespaces)
        if(len(titleNode) > 0):
            title = '<h1>' + parseAllTag(titleNode[0].text, rootImg) + '</h1>'
        else:
            title = ''
        # les champ pour les relation many to many le save de question doit se faire avant
        # on récupère et parse l'explication globale 
        globalexplanationNode = tree.xpath('/sc:item/' + path + '/sc:globalExplanation/op:res/sp:txt/op:txt', namespaces=namespaces)
        # on récupère et parse l'enoncé de la question
        textNode = tree.xpath('/sc:item/' + path + '/sc:question', namespaces=namespaces)
        textXml = etree.tostring(textNode[0], encoding='utf8').decode('utf8')
        textXmlClean = cleanXml(textXml)
        textHtml = parseAllTag(textXmlClean, rootImg)
        preview = createPreview(textHtml)
        # on récupère et parse les choix de la question
        choicesNode = tree.xpath('/sc:item/' + path + '/sc:choices/sc:choice', namespaces=namespaces)
        if(path == 'op:mcqSur'):
            solutionNode = tree.xpath('/sc:item/' + path + '/sc:solution', namespaces=namespaces)
            solution = int(solutionNode[0].attrib['choice'])
        else:
            solution = 0;
        if(len(choicesNode) > 0):
            choices = parseChoice(choicesNode, rootImg, solution)
        # contenue du fichier hmtl
        content_out_file = title + '\n' + textHtml + '\n' +  choices
        # création du fichier html
        out_file = open(outDir + code + '.html', 'w')
        # ecriture du fichier html
        out_file.write(content_out_file)
        # retourne le fichier
        return content_out_file